Vulnerability on WPA generation algorithm on Sitecom routers 
============================================================

+ Keygen for WiFi Networks manufactured by SITECOM. So far only WiFi networks with essid like SitecomXXXXXX are likely vulnerable.
  This code is able to achieve the default WPA key of those networks if you just got the wifi mac. Since there are different algorithms this tool will combine
  all possible keys depending on the mac. This tool will generate both 2.4Ghz and 5Ghz.

References
----------

* [0] Model  WLR-2500 (21 Oct 2013) : https://github.com/WarkerAnhaltRanger/EZ-Wlan/blob/master/src/de/warker/ezwlan/handler/SitecomHandler.java
* [1] Models WLM-3500 and WLM-5500  : http://blog.emaze.net/2013/08/multiple-vulnerabilities-on-sitecom.html
* [2] Models WLR-4000 and WLR-4004  : http://blog.emaze.net/2014/04/sitecom-firmware-and-wifi.html 
* [3] Models WLR2100 and all routers: http://www.ednolo.alumnos.upv.es
      which use model 341 for generating keys
* [4] "Scrutinizing WPA2 Password Generating Algorithms in Wireless Routers":  
	                                  https://www.usenix.org/conference/woot15/workshop-program/presentation/lorente

Contact
-------

Coder  : Eduardo Novella    Twitter : [@enovella_](https://twitter.com/enovella_) && 
Website: (http://ednolo.alumnos.upv.es/)


Changelog
---------
- 2015-10-18  0.2  sitecomWLR2100 class modified and public version is out!
- 2014-08-05  0.1  Start mixing all the info and 1st release


Licence
-------
GPLv3
http://gplv3.fsf.org/

More info
---------

+ http://ednolo.alumnos.upv.es

Usage
-----

	$ python sitecom.py 
		usage: sitecom.py [-h] [-b [BSSID]] [-v] [-m [MODEL] | -l | -lm]

		>>> Keygen for WiFi Networks manufactured by SITECOM. So far only WiFi networks 
		with essid like SitecomXXXXXX are likely vulnerable. This code is able to achieve
	    the default WPA key of those networks if you just got the wifi mac. Since there are
	    different algorithms this tool will combine	all possible keys depending on the mac. 
	    This tool will generate both 2.4Ghz	and 5Ghz. See http://ednolo.alumnos.upv.es/ 
	    for more details. Twitter: @enovella_ and email: ednolo[_at_]inf.upv.es

		optional arguments:
		  -h, --help            show this help message and exit
		  -v, --version         show program's version number and exit
		  -m [MODEL], --model [MODEL]
			                Get the right key giving the model
		  -l, --list            List all vulnerable targets (essid SitecomXXXXXX)
		  -lm, --listModels     List all vulnerable models and algorithms

		required:
		  -b [BSSID], --bssid [BSSID]
			                Target wifi mac address

		(+) Help: --version, --list and --listModels are mutually exclusive. Example:
		python sitecom.py -b 00:0c:f6:69:c0:de


		$ python sitecom.py -l
		[+] Possible vulnerable targets so far:
			 bssid: 00:0C:F6:XX:XX:XX 	 essid: SitecomXXXXXX
			 bssid: 64:D1:A3:XX:XX:XX 	 essid: SitecomXXXXXX


		$ python sitecom.py -lm

		###################################################################
		# Description                  ##           Model(Algorithm)      #
		###################################################################
		#                              ##                                 #
		# WLR2100 v1 001 | X2 N300     ##                2100             #
		###################################################################
		# WL328 v1 001                 ##                341              #
		# WL32X v1 001                 ##                341              #
		# WL340 v2 002                 ##                341              #
		# WL341 v2 002                 ##                341              #
		# WL341/WL582 v1 002           ##                341              #
		# WL342 v2 002                 ##                341              # 
		# WL351 v1 001                 ##                341              #
		# WL351 v1 002                 ##                341              #
		# WL351 v1 002 | X3 300N       ##                341              #
		# WL357 v1 001                 ##                341              #
		###################################################################
		# WL370   v1 001               ##                4000             #
		# WLR1000 v2 001 | X1 N150     ##                4000             #
		# WLR1100 v2 001 | X1 N150     ##                4000             #
		# WLR3001 v1 001 | X3 N300     ##                4000             #
		# WLR3100 v2 002 | X3 N300     ##                4000             #
		# WLR4000 v1 001 | X4 N300     ##                4000             #
		# WLR4003 v1 001 | X4 N300     ##                4000             #
		# WLR4100 v1 001 | X4 N300     ##                4000             #
		# WLR5000 v1 001 | X5 N600     ##                4000             #
		# WLR5100 v1 001 | X5 N600     ##                4000             #
		# WLR6000 v1 001 | X6 N750     ##                4000             #
		# WLR6100 v1 001 | X6 N900     ##                4000             #
		# WLR7100 v1 001 | X7 AC1200   ##                4000             #
		# WLR8100 v1 001 | X8 AC1750   ##                4000             #
		###################################################################
		# WLM2500 v1 001 | X2 N300     ##                2500             #
		###################################################################
		# WLM3500 v1 001 | X3 N300     ##                3500             #
		# WLM5500 v1 001 | X5 N300     ##                5500             #
		###################################################################
		# WLR4004 v1 001 | X4 N300     ##                4004             #
		###################################################################
		# WLR4200B VPN  | N300         ##                CHECK            #
		# WLR2000 v1 001| X2 N300      ##                CHECK            #
		# WLR2000 v2 001| X2 N300      ##                CHECK            #
		# WLR-4000 300N X4             ##                CHECK            #
		# WLR-5000 300N X5             ##                CHECK            #
		# WLR-6000 450N X6             ##                CHECK            #
		# WLM-4500 300N X4             ##                CHECK            #
		# WLM-5500 300N X5             ##                CHECK            #
		# WLM-5600 600N X5             ##                CHECK            #
		# WLM-6500 450N X6             ##                CHECK            #
		# WLM-6600 450N X6             ##                CHECK            #
		###################################################################
		# WL309 v1 001 Gaming router   ##                TODO             #
		# WL340 v1 002                 ##                TODO             #
		# WL350 v1 002                 ##                TODO             #
		# WL366 v1 001                 ##                TODO             #
		# WL613 v1 001                 ##                TODO             #
		# WLM3600 v1 001 | X3 N300     ##                TODO             #
		###################################################################


		$ python sitecom.py -b 000cf6123456
		YQM0R62IGT97
		BUQ2V84LJXB9
		BUEJVQLA8XTR
		JFJSHBVACHKE
		yUY1BJPy
		XjjVn511
		UC2CGFyg
		V1Mn4maC
		49TXT5P3YQW7
		6BX8X7T5BU39
		NTX8XPHMBU3R
		SPXTNCXRG7A6
		WT8XRE8VJ9C8
		WH8XF48V8R2Q

		$ python sitecom.py -b 000cf6123456 -m 341
		YQM0R62IGT97
		49TXT5P3YQW7
		SPXTNCXRG7A6
